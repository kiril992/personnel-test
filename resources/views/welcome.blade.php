@extends('layouts.app')

@section('content')

    <div class="row justify-content-between">
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">call</i>
                    </div>
                    <p class="card-category">Calls</p>
                    <h3 class="card-title">{{ $calls }}
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <div>
                            <a href="{{ route('calls.index') }}" class="mr-2 btn btn-sm btn-warning">Manage Calls</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">person</i>
                    </div>
                    <p class="card-category">Users</p>
                    <h3 class="card-title">{{ $users }}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <a href="{{ route('users.index') }}" class="btn btn-sm btn-info ml-2">Manage Users</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">person_search</i>
                    </div>
                    <p class="card-category">Clients</p>
                    <h3 class="card-title">{{ $clients }}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <a href="{{ route('clients.index') }}" class="btn btn-sm btn-success">Manage Clients</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection