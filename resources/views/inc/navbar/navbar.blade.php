<nav class="navbar fixed-top navbar-expand-lg bg-light">
    <div class="container">
        <a class="navbar-brand" href="{{ route('welcome') }}">Personnel LTD</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-bar navbar-kebab"></span>
      <span class="navbar-toggler-bar navbar-kebab"></span>
      <span class="navbar-toggler-bar navbar-kebab"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item {{ Request::segment(1)  == "calls" ? "active" : "" }}">
            <a class="nav-link" href="{{ route('calls.index') }}">Calls <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item {{ Request::segment(1)  == "users" ? "active" : "" }}">
            <a class="nav-link" href="{{ route('users.index') }}">Users</a>
          </li>
          <li class="nav-item {{ Request::segment(1)  == "clients" ? "active" : "" }}">
            <a class="nav-link" href="{{ route('clients.index') }}">Clients</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>