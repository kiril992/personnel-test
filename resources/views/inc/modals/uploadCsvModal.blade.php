<!-- Modal  -->
<div class="modal fade" id="importCallsModal" tabindex="-1" role="dialog" aria-labelledby="importCallsModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Upload calls in this order.  /Example Below.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>User | Client | Client Type | Date | Duration | Type Of Call | External Call Score</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <tr>
                            <td>
                                John Doe,Jane Doe,Developer,2020-11-11,100,Incoming,500
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="card-footer">
                <form method="POST" action="{{ route('calls.import') }}" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="d-flex justify-content-between">
                        <div class="form-group">
                            <label class="border text-primary rounded p-2 submit-csv-label" for="file" title="Choose file to upload">CHOOSE FILE</label>
                            <input type="file" name="file" id="file" hidden>
                        </div>
                        
                        <div>
                            <button type="submit" class="btn">Import Calls</button>
                        </div>
                    </div>

                </form>
            </div>
            
        </div>
    </div>
</div>