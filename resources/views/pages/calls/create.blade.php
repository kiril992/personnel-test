@extends('layouts.app')
    

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    
    <style>
        .actions {
            display: fixed;
        }
        .hidden {
            display: none;
        }
        .filter-option-inner-inner, .dropdown-toggle:after{
            color: #999999 !important;
        }

        .dropdown-menu li a:hover{
            background-color: #4db6ac !important;
        }

        .btn-link {
            background-color: transparent !important;
        }

        .btn-active {
            background: #999999!important;
            color: white!important;
        }
    </style>

@endsection

@section('content')

@include('inc.errors.error')
@include('inc.successes.success')

<div class="row">
    <div class="col-md-8 offset-2">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <h4 class="card-title">Create Call</h4>
                        <p class="card-category">Complete all fields</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('calls.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-static">Date</label>
                                <input type="text" class="form-control datetimepicker" name="date" required value="{{ old('date') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-static">Duration</label>
                                <input type="text" class="form-control" name="duration" required value="{{ old('duration') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-static">External call score</label>
                                <input type="text" class="form-control" name="external_call_score" required value="{{ old('external_call_score') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="selectUser">Select user</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="selectUser" name="user_id" required>
                                    <option value="" selected disabled>Please select</option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="selectUser">Select client</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="selectClient" name="client_id" required>
                                    <option value="" selected disabled>Please select</option>
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}">{{ $client->full_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="selectUser">Select Type of call</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="type_of_call" name="type_of_call" required>
                                    <option value="" selected disabled>Please select</option>
                                    @foreach($typeOfCall as $type)
                                        <option value="{{ $type }}">{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection

