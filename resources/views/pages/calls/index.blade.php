@extends('layouts.app')

@section('content')

    @include('inc.errors.error')
    @include('inc.successes.success')

    <div class="row">
        <div class="card card-nav-tabs card-plain">
            
            <div class="card-header card-header-primary">
                <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs pull-left">
                            <li class="nav-item">
                               <button type="button" class="btn btn-sm btn-secondary text-primary" data-toggle="modal" data-target="#importCallsModal">Upload CSV</button>
                                <a href="{{ route('calls.create') }}" class="btn btn-sm btn-secondary text-primary">Create</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="tab-content text-center">
                    <div class="tab-pane active" id="calls">
                        <table class="table">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Client</th>
                                    <th>Client Type</th>
                                    <th>Date</th>
                                    <th>Duration</th>
                                    <th>Type Of Call</th>
                                    <th>External Call Score</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>

                                @forelse ($calls as $count => $call)
                                    
                                    <tr>
                                        <td>{{ $count + 1 }}</td>
                                        <td>{{ $call->user->name }}</td>
                                        <td>{{ $call->client->full_name }}</td>
                                        <td>{{ $call->client->client_type }}</td>
                                        <td>{{ $call->date }}</td>
                                        <td>{{ $call->duration }}</td>
                                        <td>{{ $call->type_of_call }}</td>
                                        <td>{{ $call->external_call_score }}</td>
                                        <td>
                                            <a href="{{ route('calls.edit', $call->id) }}" rel="tooltip" class="btn btn-warning btn-link btn-sm p-0" title="Edit">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <button type="button" rel="tooltip" class="btn btn-danger btn-link btn-sm p-0" title="Delete" 
                                                    data-toggle="modal" data-target="#deleteModalCall{{$call->id}}">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </td>
                                    </tr>

                                    {{-- DELETE MODAL --}}
                                    @include('inc.modals.deleteModal', [
                                        'route' => route('calls.destroy', $call->id),
                                        'deleteModalId' => 'deleteModalCall'.$call->id,
                                        'deleteModalLabel' => 'deleteModalLabelCall',
                                        'item' => 'Call'
                                    ])

                                @empty
                                    <h4>No records!</h4>
                                @endforelse

                            </tbody>

                        </table>

                        <hr>
                        <div class="d-flex justify-content-between">
                            <span>{{ $calls->links() }}</span>
                            <span> <strong>Total:</strong> {{ $calls->total() }}</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- CSV UPLOAD MODAL --}}
    @include('inc.modals.uploadCsvModal')
    
@endsection