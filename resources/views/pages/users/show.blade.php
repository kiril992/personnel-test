@extends('layouts.app')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

    @include('inc.errors.error')
    @include('inc.successes.success')

    <div class="row">
        <div class="card card-nav-tabs card-plain">
            
            <div class="card-header card-header-primary">
                <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs pull-left">
                            <li class="nav-item">
                                <h4>
                                    User: {{$user->name}}
                                </h4>
                                <hr class="m-0 bg-white">
                                <h4>
                                    Average Call Score: {{ $avgCallScore }}
                                </h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="tab-content text-center">
                    <div class="tab-pane active" id="users">
                        <table class="table">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Client Type</th>
                                    <th>Date</th>
                                    <th>Duration</th>
                                    <th>Type Of Call</th>
                                    <th>External Call Score</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($user->calls as $count => $call)
                                    
                                    <tr>
                                        <td>{{ $count + 1 }}</td>
                                        <td>{{ $call->client->full_name }}</td>
                                        <td>{{ $call->client->client_type }}</td>
                                        <td>{{ $call->date }}</td>
                                        <td>{{ $call->duration }}</td>
                                        <td>{{ $call->type_of_call }}</td>
                                        <td>{{ $call->external_call_score }}</td>
                                    </tr>

                                @endforeach
                            </tbody>

                        </table>
                        <hr>
                        <div class="d-flex justify-content-between">
                            <span>{{ $user->calls->links() }}</span>
                            <span> <strong>Total:</strong> {{ $user->calls->total() }}</span>
                        </div>

                    </div>
                </div>
            </div>

          </div>
    </div>
    
@endsection