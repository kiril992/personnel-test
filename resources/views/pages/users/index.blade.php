@extends('layouts.app')

@section('content')

    @include('inc.errors.error')
    @include('inc.successes.success')

    <div class="row">
        <div class="card card-nav-tabs card-plain">
            
            <div class="card-header card-header-primary">
                <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs pull-left">
                            <li class="nav-item">
                                <a href="{{ route('users.create') }}" class="btn btn-sm btn-secondary text-primary">Create</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="tab-content text-center">
                    <div class="tab-pane active" id="users">
                        <table class="table">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>

                                @forelse ($users as $count => $user)
                                    
                                    <tr>
                                        <td>{{ $count + 1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        
                                        <td>
                                            <a href="{{ route('users.show', $user->id) }}" rel="tooltip" class="btn btn-info btn-link btn-sm p-0" title="Show Stats">
                                                <i class="material-icons">remove_red_eye</i>
                                            </a>
                                            <a href="{{ route('users.edit', $user->id) }}" rel="tooltip" class="btn btn-warning btn-link btn-sm p-0" title="Edit">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <button type="button" rel="tooltip" class="btn btn-danger btn-link btn-sm p-0" title="Delete" 
                                                    data-toggle="modal" data-target="#deleteModalUser{{$user->id}}">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </td>
                                    </tr>

                                    {{-- DELETE MODAL --}}
                                    @include('inc.modals.deleteModal', [
                                        'route' => route('users.destroy', $user->id),
                                        'deleteModalId' => 'deleteModalUser'.$user->id,
                                        'deleteModalLabel' => 'deleteModalLabelUser',
                                        'item' => 'User',
                                    ])

                                @empty
                                    <h4>No records!</h4>
                                @endforelse

                            </tbody>

                        </table>

                        <hr>
                        <div class="d-flex justify-content-between">
                            <span>{{ $users->links() }}</span>
                            <span> <strong>Total:</strong> {{ $users->total() }}</span>
                        </div>

                    </div>
                </div>
            </div>

          </div>
    </div>
    
@endsection