@extends('layouts.app')

@section('content')

    @include('inc.errors.error')
    @include('inc.successes.success')

    <div class="row">
        <div class="card card-nav-tabs card-plain">
            
            <div class="card-header card-header-primary">
                <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs pull-left">
                            <li class="nav-item">
                                <a href="{{ route('clients.create') }}" class="btn btn-sm btn-secondary text-primary">Create</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="tab-content text-center">
                    <div class="tab-pane active" id="clients">
                        <table class="table">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>

                                @forelse ($clients as $count => $client)
                                    
                                    <tr>
                                        <td>{{ $count + 1 }}</td>
                                        <td>{{ $client->full_name }}</td>
                                        
                                        <td>
                                            <a href="{{ route('clients.edit', $client->id) }}" rel="tooltip" class="btn btn-warning btn-link btn-sm p-0" title="Edit">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <button type="button" rel="tooltip" class="btn btn-danger btn-link btn-sm p-0" title="Delete" 
                                                    data-toggle="modal" data-target="#deleteModalClient{{$client->id}}">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </td>
                                    </tr>

                                    {{-- DELETE MODAL --}}
                                    @include('inc.modals.deleteModal', [
                                        'route' => route('clients.destroy', $client->id),
                                        'deleteModalId' => 'deleteModalClient'.$client->id,
                                        'deleteModalLabel' => 'deleteModalLabelClient',
                                        'item' => 'Client'
                                    ])

                                @empty
                                    <h4>No records!</h4>
                                @endforelse

                            </tbody>

                        </table>

                        <hr>
                        <div class="d-flex justify-content-between">
                            <span>{{ $clients->links() }}</span>
                            <span> <strong>Total:</strong> {{ $clients->total() }}</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection