@extends('layouts.app')
    
@section('content')

@include('inc.errors.error')
@include('inc.successes.success')

<div class="row">
    <div class="col-md-6 offset-3">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <h4 class="card-title">Edit</h4>
                        <p class="card-category">Complete all fields</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('clients.update', $client->id) }}">
                    @csrf
                    @method('PUT')
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-static">Clent full name</label>
                                <input type="text" class="form-control" name="full_name" required value="{{ $client->full_name }}">
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
</div>

@endsection

