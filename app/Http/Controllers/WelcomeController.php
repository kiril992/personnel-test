<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Call;
use App\User;
use App\Client;

class WelcomeController extends Controller
{
    public function index()
    {
        // Count records for each model...
        $calls = Call::count();
        $users = User::count();
        $clients = Client::count();

        return view('welcome', compact('calls', 'users', 'clients'));
    }
}
