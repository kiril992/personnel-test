<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use App\Client;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::latest()->paginate(10);

        return view('pages.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        $dataToStore = $request->validated();

        $success = Client::create($dataToStore);

        if($success) {
            
            return redirect()->route('clients.index')->withSuccess('Success: Client was created successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('pages.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, Client $client)
    {
        $dataToStore = $request->validated();

        $success = $client->update($dataToStore);

        if($success) {

            return redirect()->route('clients.index')->withSuccess('Success: Client was updated successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //If this client have calls under it, dont allow to be deleted...
        if(count($client->calls)) {
            
            return redirect()->back()
                ->withErrors(['warning' => "Warning: Can't delete client, has calls under it!"]);
        }

        if($client->delete()) {

            return redirect()->back()->withSuccess('Success: Client was deleted successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
    }
}
