<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CallRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CallsImport;
use App\Call;
use App\User;
use App\Client;
use Illuminate\Support\Facades\Storage;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calls = Call::with('user', 'client')
                    ->latest()
                    ->paginate(10);

        return view('pages.calls.index', compact('calls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $clients = Client::all();
        $typeOfCall = ['Incoming', 'Outgoing'];

        return view('pages.calls.create', compact('users','clients','typeOfCall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CallRequest $request)
    {
        $dataToStore = $request->validated();

        $success = Call::create($dataToStore);

        if($success) {

            return redirect()->route('calls.index')->withSuccess('Success: Call was created successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Call $call)
    {
        $users = User::all();
        $clients = Client::all();
        $typeOfCall = ['Incoming', 'Outgoing'];

        return view('pages.calls.edit', compact('call', 'users','clients','typeOfCall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CallRequest $request, Call $call)
    {
        $dataToUpdate = $request->validated();

        $success = $call->update($dataToUpdate);

        if($success) {

            return redirect()->route('calls.index')->withSuccess('Success: Call was updated successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Call $call)
    {
        $success = $call->delete();
        
        if($success) {

            return redirect()->back()->withSuccess('Success: Call was deleted successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
    }

    public function importCall(Request $request)
    {
        //Valdate data
        $dataToStore = $request->validate([
            'file' => 'required|mimes:csv,txt',
        ]);

        // Store file data localy
        $storedCallsPath = $request->file('file')->store('imported_calls');

        try {

            // Take stored file data, and put through CallsImport class, so that we can save it to the database
            Excel::import(new CallsImport, $storedCallsPath);

            // Delete stored file data, if saving to databas was successfull...
            Storage::deleteDirectory('imported_calls');

            return redirect()->back()->withSuccess('Success: file was uploaded successfully!');

        } catch (\Exception $e) {
            
            return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
        }

    }
}
