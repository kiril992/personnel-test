<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('calls.client')
                    ->latest()
                    ->paginate(10);

        return view('pages.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $dataToStore = $request->validated();

        $success = User::create($dataToStore);

        if($success) {

            return redirect()->route('users.index')->withSuccess('Success: Call was created successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //setRelation() function are allowing us to make pagination to calls relationship...
        $user = $user->load('calls.client')
                ->setRelation('calls', $user->calls()
                ->latest()
                ->paginate(5));

        // Take external_call_score for user, and calucate average points...
        $avgCallScore = round($user->calls()->avg('external_call_score'), 1);

        return view('pages.users.show', compact('user', 'avgCallScore'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('pages.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $dataToUpdate = $request->validated();

        $success = $user->update($dataToUpdate);

        if($success) {

            return redirect()->route('users.index')->withSuccess('Success: User was updated successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //If this user have calls under it, dont allow to be deleted...
        if(count($user->calls)) {
            
            return redirect()->back()
                ->withErrors(['warning' => "Warning: Can't delete user, has calls under it!"]);
        }

        if($user->delete()) {

            return redirect()->back()->withSuccess('Success: Call was deleted successfully!');
        }

        return redirect()->back()->withErrors('error', 'Ups! Something went wrong, please try again!');
    }
}
