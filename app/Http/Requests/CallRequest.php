<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'duration' => 'required|integer',
            'type_of_call' => 'required',
            'external_call_score' => 'required|integer',
            'user_id' => 'required|integer',
            'client_id' => 'required|integer',
        ];
    }
}
