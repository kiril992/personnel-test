<?php

namespace App\Imports;

use App\User;
use App\Client;
use App\Call;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CallsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // If user is already in database, ignore it... if there is no such user, save it to the database
        $user = User::firstOrCreate([
            'name' => $row['user'],
        ]);

        // If user is already in database, ignore it... if there is no such client, save it to the database
        $client = Client::firstOrCreate([
            'full_name'   => $row['client'],
            'client_type' => $row['client_type'],
        ]);

        // Store calls with created user, and client...
        $calls = Call::create([
            'user_id'             => $user->id,
            'client_id'           => $client->id,
            'date'                => $row['date'],
            'duration'            => $row['duration'],
            'type_of_call'        => $row['type_of_call'],
            'external_call_score' => $row['external_call_score'],
        ]);
        
    }
}
