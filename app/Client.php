<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['full_name', 'client_type'];

    public function calls()
    {
        return $this->hasMany(Call::class);
    }
}
