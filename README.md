# Getting started

## Installation

### Install all the dependencies using composer

```composer install```

### Copy the example env file and make the required configuration changes in the .env file

```cp .env.example .env```

### Set the database connection in the .env file

```
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
### Generate a new application key

```php artisan key:generate```

### Run the database migrations

```php artisan migrate```

### Start the local development server

```php artisan serve```

### You can now access the server at http://localhost:8000
