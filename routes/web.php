<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',                       'WelcomeController@index')->name('welcome');

// ================================= RESOURCE ROUTES
Route::resource('calls',  'CallController');
Route::resource('users',  'UserController');
Route::resource('clients', 'ClientController');

// ===================================== IMPORT ROUTE
Route::post('/calls/import',           'CallController@importCall')->name('calls.import');
